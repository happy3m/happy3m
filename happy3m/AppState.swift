//
//  AppState.swift
//  happy3m
//
//  Created by k.ohira on 2016/10/10.
//  Copyright © 2016年 k.ohira. All rights reserved.
//

import Foundation

class AppState: NSObject {
    
    static let sharedInstance = AppState()
    
    var signedIn = false
    var displayName: String?
    var photoURL: URL?
}
