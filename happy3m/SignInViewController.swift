//
//  SignInViewController.swift
//  happy3m
//
//  Created by k.ohira on 2016/10/10.
//  Copyright © 2016年 k.ohira. All rights reserved.
//

import UIKit

import Firebase


@objc(SignInViewController)
class SignInViewController: UIViewController {
        
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @IBAction func didTapSignIn(_ sender: AnyObject) {
        signedIn(nil)
    }
    
    @IBAction func didTapSignUp(_ sender: AnyObject) {
        setDisplayName(nil)
    }
    
    func setDisplayName(_ user: FIRUser?) {
        signedIn(nil)
    }
    
    @IBAction func didRequestPasswordReset(_ sender: AnyObject) {
    }
    
    func signedIn(_ user: FIRUser?) {
        MeasurementHelper.sendLoginEvent()
        
        AppState.sharedInstance.signedIn = true
        let notificationName = Notification.Name(rawValue: Constants.NotificationKeys.SignedIn)
        NotificationCenter.default.post(name: notificationName, object: nil, userInfo: nil)
        performSegue(withIdentifier: Constants.Segues.SignInToFp, sender: nil)
    }    
}
